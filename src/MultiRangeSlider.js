
import React, {useEffect, useState, useRef} from "react";
import './multiRangeSlider.css';

const MultiRangeSlider = ({min, max}) => {
const [minVal, setMinVal] = useState(min);
const [maxVal, setMaxVal] = useState(max);
const range = useRef(null);

useEffect(()=> setLeftValue(), [minVal]);

useEffect(()=> setRightValue(), [maxVal]);

const getPercent = value => Math.round(((value - min) / (max - min)) * 100);

const setLeftValue = () => {
  const minPercent = getPercent(minVal);
  const maxPercent = getPercent(maxVal);
  
  if(range.current) {
    range.current.style.left = `${minPercent}%`;
    range.current.style.width = `${maxPercent - minPercent}%`;
  }
}

const setRightValue = () => {
  const minPercent = getPercent(minVal);
  const maxPercent = getPercent(maxVal);

  if (range.current) {
    range.current.style.width = `${maxPercent - minPercent}%`;
  }
}

return (
    <>
      <input
        type="range"
        min={min}
        max={max}
        value={minVal}
        onChange={event => setMinVal(Math.min(Number(event.target.value), maxVal - 1))}
        className="thumb thumb--left"
      />
      <input
        type="range"
        min="0"
        max="1000"
        value={maxVal}
        onChange={event => setMaxVal(Math.max(Number(event.target.value), minVal + 1))}
        className="thumb thumb--right"
        style={{ zIndex: minVal > max - 100 && '5'}}
      />
      <div className="slider">
      <div className="slider__track" />
      <div ref={range} className="slider__range" />
      <div className='slider__left-value'>{minVal}</div>
      <div className='slider__right-value'>{maxVal}</div>
</div>

    </>
  );
};

export default MultiRangeSlider;

