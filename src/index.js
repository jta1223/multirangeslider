import React from "react";
import ReactDOM from "react-dom";
import MultiRangeSlider from "./MultiRangeSlider";

var mountNode = document.getElementById("app");
ReactDOM.render(<MultiRangeSlider min={0} max={1000}/>, mountNode);